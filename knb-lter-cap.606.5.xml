<?xml version="1.0" encoding="UTF-8"?>
<eml:eml xmlns:eml="https://eml.ecoinformatics.org/eml-2.2.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:stmml="http://www.xml-cml.org/schema/stmml-1.2" packageId="knb-lter-cap.606.5" scope="system" system="knb" xsi:schemaLocation="https://eml.ecoinformatics.org/eml-2.2.0 https://eml.ecoinformatics.org/eml-2.2.0/eml.xsd">
  <access authSystem="knb" order="allowFirst" scope="document">
    <allow>
      <principal>uid=CAP,o=EDI,dc=edirepository,dc=org</principal>
      <permission>all</permission>
    </allow>
    <allow>
      <principal>public</principal>
      <permission>read</permission>
    </allow>
  </access>
  <dataset>
    <title>Long-term monitoring of streamwater chemistry in Sycamore Creek, Arizona, USA (2010-2014)</title>
    <creator>
      <individualName>
        <givenName>Nancy</givenName>
        <surName>Grimm</surName>
      </individualName>
      <organizationName>Arizona State University</organizationName>
      <electronicMailAddress>nbgrimm@asu.edu</electronicMailAddress>
      <userId directory="https://orcid.org">0000-0001-9374-660X</userId>
    </creator>
    <creator>
      <individualName>
        <givenName>John</givenName>
        <surName>Sabo</surName>
      </individualName>
      <organizationName>Arizona State University</organizationName>
      <electronicMailAddress>John.L.Sabo@asu.edu</electronicMailAddress>
      <userId directory="https://orcid.org">0000-0001-5259-0709</userId>
    </creator>
    <creator>
      <individualName>
        <givenName>Sophia</givenName>
        <surName>Bonjour</surName>
      </individualName>
      <organizationName>Arizona State University</organizationName>
      <electronicMailAddress>sbonjour@ksu.edu</electronicMailAddress>
      <userId directory="https://orcid.org">0000-0003-3614-7023</userId>
    </creator>
    <creator>
      <individualName>
        <givenName>Xiaoli</givenName>
        <surName>Dong</surName>
      </individualName>
      <organizationName>University of California, Davis</organizationName>
      <electronicMailAddress>xldong@ucdavis.edu</electronicMailAddress>
      <userId directory="https://orcid.org">0000-0003-3303-0735</userId>
    </creator>
    <creator>
      <individualName>
        <givenName>Amalia</givenName>
        <surName>Handler</surName>
      </individualName>
      <organizationName>USEPA National Health and Environmental Effects</organizationName>
      <electronicMailAddress>Handler.amalia@Epa.gov</electronicMailAddress>
      <userId directory="https://orcid.org">0000-0001-8372-6488</userId>
    </creator>
    <creator>
      <individualName>
        <givenName>Kathrine</givenName>
        <surName>Kemmitt</surName>
      </individualName>
      <organizationName>Wenck</organizationName>
      <electronicMailAddress>kkemmitt@wenck.com</electronicMailAddress>
      <userId directory="https://orcid.org">0000-0002-3794-8941</userId>
    </creator>
    <creator>
      <individualName>
        <givenName>Marina</givenName>
        <surName>Lauck</surName>
      </individualName>
      <organizationName>Arizona State University</organizationName>
      <electronicMailAddress>Marina.Lauck@asu.edu</electronicMailAddress>
      <userId directory="https://orcid.org">0000-0002-9032-0782</userId>
    </creator>
    <creator>
      <individualName>
        <givenName>Monica</givenName>
        <surName>Palta</surName>
      </individualName>
      <organizationName>Pace University</organizationName>
      <electronicMailAddress>mpalta@pace.edu</electronicMailAddress>
    </creator>
    <creator>
      <individualName>
        <givenName>Lindsey</givenName>
        <surName>Pollard</surName>
      </individualName>
      <organizationName>Arizona State University</organizationName>
      <electronicMailAddress>ldpollar@gmail.com</electronicMailAddress>
    </creator>
    <metadataProvider>
      <individualName>
        <givenName>Nancy</givenName>
        <surName>Grimm</surName>
      </individualName>
      <organizationName>Arizona State University</organizationName>
      <electronicMailAddress>nbgrimm@asu.edu</electronicMailAddress>
      <userId directory="https://orcid.org">0000-0001-9374-660X</userId>
    </metadataProvider>
    <metadataProvider>
      <individualName>
        <givenName>Lindsey</givenName>
        <surName>Pollard</surName>
      </individualName>
      <organizationName>Arizona State University</organizationName>
      <electronicMailAddress>ldpollar@gmail.com</electronicMailAddress>
    </metadataProvider>
    <metadataProvider>
      <individualName>
        <givenName>Sophia</givenName>
        <surName>Bonjour</surName>
      </individualName>
      <organizationName>Arizona State University</organizationName>
      <electronicMailAddress>sbonjour@ksu.edu</electronicMailAddress>
      <userId directory="https://orcid.org">0000-0003-3614-7023</userId>
    </metadataProvider>
    <pubDate>2020-12-17</pubDate>
    <language>english</language>
    <abstract>
      <markdown>
The primary objective of this project is to understand how long-term climate variability and change influence the structure and function of desert streams via effects on hydrologic disturbance regimes. Climate and hydrology are intimately linked in arid landscapes; for this reason, desert streams are particularly well suited for both observing and understanding the consequences of climate variability and directional change. Researchers try to (1) determine how climate variability and change over multiple years influence stream biogeomorphic structure (i.e., prevalence and persistence of wetland and gravel-bed ecosystem states) via their influence on factors that control vegetation biomass, and (2) compare interannual variability in within-year successional patterns in ecosystem processes and community structure of primary producers and consumers of two contrasting reach types (wetland and gravel-bed stream reaches). This specific dataset was collected to monitor long-term changes in dissolved nutrient concentrations (N, P, C) by sampling surface water within gravel and wetland dominated reaches during baseflow.
</markdown>
    </abstract>
    <keywordSet>
      <keyword keywordType="theme">organic carbon</keyword>
      <keyword keywordType="theme">dissolved organic carbon</keyword>
      <keyword keywordType="theme">phosphate</keyword>
      <keyword keywordType="theme">phosphorus</keyword>
      <keyword keywordType="theme">nitrogen</keyword>
      <keyword keywordType="theme">nitrate</keyword>
      <keyword keywordType="theme">ammonium</keyword>
      <keyword keywordType="theme">total dissolved nitrogen</keyword>
      <keyword keywordType="theme">organic nitrogen</keyword>
      <keyword keywordType="theme">inorganic nitrogen</keyword>
      <keyword keywordType="theme">nutrients</keyword>
      <keyword keywordType="theme">deserts</keyword>
      <keyword keywordType="theme">aquatic ecosystems</keyword>
      <keyword keywordType="theme">drainage</keyword>
      <keyword keywordType="theme">droughts</keyword>
      <keyword keywordType="theme">streams</keyword>
      <keywordThesaurus>LTER controlled vocabulary</keywordThesaurus>
    </keywordSet>
    <keywordSet>
      <keyword keywordType="theme">population studies</keyword>
      <keyword keywordType="theme">disturbance patterns</keyword>
      <keyword keywordType="theme">water and fluxes</keyword>
      <keywordThesaurus>LTER core areas</keywordThesaurus>
    </keywordSet>
    <keywordSet>
      <keyword keywordType="place">sycamore creek</keyword>
      <keyword keywordType="place">sonoran desert</keyword>
      <keyword keywordType="place">LTREB</keyword>
      <keywordThesaurus>Creator Defined Keyword Set</keywordThesaurus>
    </keywordSet>
    <keywordSet>
      <keyword keywordType="theme">cap lter</keyword>
      <keyword keywordType="theme">cap</keyword>
      <keyword keywordType="theme">caplter</keyword>
      <keyword keywordType="theme">central arizona phoenix long term ecological research</keyword>
      <keyword keywordType="place">arizona</keyword>
      <keyword keywordType="place">az</keyword>
      <keyword keywordType="theme">arid land</keyword>
      <keywordThesaurus>CAPLTER Keyword Set List</keywordThesaurus>
    </keywordSet>
    <intellectualRights>This data package is released to the "public domain" under Creative Commons CC0 1.0 "No Rights Reserved" (see: https://creativecommons.org/publicdomain/zero/1.0/). The consumer of these data ("Data User" herein) has an ethical obligation to cite it appropriately in any publication that results from its use. The Data User should realize that these data may be actively used by others for ongoing research and that coordination may be necessary to prevent duplicate publication. The Data User is urged to contact the authors of these data if any questions about methodology or results occur. Where appropriate, the Data User is encouraged to consider collaboration or coauthorship with the authors. The Data User should realize that misinterpretation of data may occur if used out of context of the original study. While substantial efforts are made to ensure the accuracy of data and associated documentation, complete accuracy of data sets cannot be guaranteed. All data are made available "as is". The Data User should be aware, however, that data are updated periodically and it is the responsibility of the Data User to check for new versions of the data. The data authors and the repository where these data were obtained shall not be liable for damages resulting from any use or misinterpretation of the data. Thank you.</intellectualRights>
    <distribution>
      <online>
        <onlineDescription>CAPLTER Metadata URL</onlineDescription>
        <url>https://sustainability.asu.edu/caplter/data/data-catalog/view/knb-lter-cap.606.5/xml/</url>
      </online>
    </distribution>
    <coverage>
      <geographicCoverage>
        <geographicDescription>SG: gravel reach of the Dos S sampling site of Sycamore Creek in central Arizona, USA</geographicDescription>
        <boundingCoordinates>
          <westBoundingCoordinate>-111.507645</westBoundingCoordinate>
          <eastBoundingCoordinate>-111.506849</eastBoundingCoordinate>
          <northBoundingCoordinate>+33.750482</northBoundingCoordinate>
          <southBoundingCoordinate>+33.749021</southBoundingCoordinate>
        </boundingCoordinates>
      </geographicCoverage>
      <geographicCoverage>
        <geographicDescription>SW: wetland reach of the Dos S sampling site of Sycamore Creek in central Arizona, USA</geographicDescription>
        <boundingCoordinates>
          <westBoundingCoordinate>-111.510883</westBoundingCoordinate>
          <eastBoundingCoordinate>-111.50779</eastBoundingCoordinate>
          <northBoundingCoordinate>+33.748620</northBoundingCoordinate>
          <southBoundingCoordinate>+33.74835</southBoundingCoordinate>
        </boundingCoordinates>
      </geographicCoverage>
      <geographicCoverage>
        <geographicDescription>RV, RG, RW and ABOVE RG: Round Valley sampling area of Sycamore Creek in central Arizona, USA</geographicDescription>
        <boundingCoordinates>
          <westBoundingCoordinate>-111.509867</westBoundingCoordinate>
          <eastBoundingCoordinate>-111.498464</eastBoundingCoordinate>
          <northBoundingCoordinate>+33.794350</northBoundingCoordinate>
          <southBoundingCoordinate>+33.778924</southBoundingCoordinate>
        </boundingCoordinates>
      </geographicCoverage>
      <geographicCoverage>
        <geographicDescription>BG and BW: Below Mesquite Wash sampling area of Sycamore Creek in central Arizona, USA</geographicDescription>
        <boundingCoordinates>
          <westBoundingCoordinate>-111.527569</westBoundingCoordinate>
          <eastBoundingCoordinate>-111.514162</eastBoundingCoordinate>
          <northBoundingCoordinate>+33.735588</northBoundingCoordinate>
          <southBoundingCoordinate>+33.724719</southBoundingCoordinate>
        </boundingCoordinates>
      </geographicCoverage>
      <temporalCoverage>
        <rangeOfDates>
          <beginDate>
            <calendarDate>2010-04-15</calendarDate>
          </beginDate>
          <endDate>
            <calendarDate>2014-06-24</calendarDate>
          </endDate>
        </rangeOfDates>
      </temporalCoverage>
    </coverage>
    <contact>
      <organizationName>Central Arizona–Phoenix LTER</organizationName>
      <positionName>Information Manager</positionName>
      <address>
        <deliveryPoint>Arizona State University</deliveryPoint>
        <deliveryPoint>Global Institute of Sustainability</deliveryPoint>
        <city>Tempe</city>
        <administrativeArea>AZ</administrativeArea>
        <postalCode>85287-5402</postalCode>
        <country>USA</country>
      </address>
      <electronicMailAddress>caplter.data@asu.edu</electronicMailAddress>
      <onlineUrl>https://sustainability.asu.edu/caplter/</onlineUrl>
    </contact>
    <publisher>
      <organizationName>Central Arizona–Phoenix LTER</organizationName>
      <address>
        <deliveryPoint>Arizona State University</deliveryPoint>
        <deliveryPoint>Global Institute of Sustainability</deliveryPoint>
        <city>Tempe</city>
        <administrativeArea>AZ</administrativeArea>
        <postalCode>85287-5402</postalCode>
        <country>USA</country>
      </address>
    </publisher>
    <methods>
      <methodStep>
        <description>
          <markdown>
*Baseline samples:*

Collected both upstream (of spring sequence injection point) and downstream of each gravel and wetland study reach. Samples were filtered in the lab using ashed Whatman 47mm GFF filters. Filtrate was stored in Falcon tubes and frozen until analyzed.

*Spring sequence samples (April-July):*

NO3, SRP samples were field filtered using an ashed Whatman GFF (2010-2011) or a .45 um membrane syringe filter (since 2012). DOC/TN samples were field-filtered using an ashed Whatman GFF filter. Filtrate was stored in Falcon tubes and frozen until analyzed.  NO3, NH4, and SRP were analyzed using flow injection analysis (Lachat QC8000). DOC and TN were analyzed using total organic carbon/nitrogen analysis (Shimadzu TOV-C). Known standard concentrations and blank samples were run every 10 field samples to ensure quality control.
</markdown>
        </description>
      </methodStep>
    </methods>
    <project>
      <title>Central Arizona–Phoenix Long-Term Ecological Research Project</title>
      <personnel>
        <individualName>
          <givenName>Daniel</givenName>
          <surName>Childers</surName>
        </individualName>
        <organizationName>Arizona State University</organizationName>
        <electronicMailAddress>dan.childers@asu.edu</electronicMailAddress>
        <userId directory="https://orcid.org">0000-0003-3904-0803</userId>
        <role>Principal Investigator</role>
      </personnel>
      <personnel>
        <individualName>
          <givenName>Nancy</givenName>
          <surName>Grimm</surName>
        </individualName>
        <organizationName>Arizona State University</organizationName>
        <electronicMailAddress>nbgrimm@asu.edu</electronicMailAddress>
        <userId directory="https://orcid.org">0000-0001-9374-660X</userId>
        <role>Co-principal Investigator</role>
      </personnel>
      <personnel>
        <individualName>
          <givenName>Sharon</givenName>
          <surName>Hall</surName>
        </individualName>
        <organizationName>Arizona State University</organizationName>
        <electronicMailAddress>sharonjhall@asu.edu</electronicMailAddress>
        <userId directory="https://orcid.org">0000-0002-8859-6691</userId>
        <role>Co-principal Investigator</role>
      </personnel>
      <personnel>
        <individualName>
          <givenName>Billie</givenName>
          <surName>Turner II</surName>
        </individualName>
        <organizationName>Arizona State University</organizationName>
        <electronicMailAddress>Billie.L.Turner@asu.edu</electronicMailAddress>
        <userId directory="https://orcid.org">0000-0002-6507-521X</userId>
        <role>Co-principal Investigator</role>
      </personnel>
      <personnel>
        <individualName>
          <givenName>Abigail</givenName>
          <surName>York</surName>
        </individualName>
        <organizationName>Arizona State University</organizationName>
        <electronicMailAddress>Abigail.York@asu.edu</electronicMailAddress>
        <userId directory="https://orcid.org">0000-0002-2313-9262</userId>
        <role>Co-principal Investigator</role>
      </personnel>
      <abstract>Phase IV of the Central Arizona-Phoenix LTER (CAP) continues to focus on the question: How do the ecosystem services provided by urban ecological infrastructure (UEI) affect human outcomes and behavior, and how do human actions affect patterns of urban ecosystem structure and function and, ultimately, urban sustainability and resilience? The overarching goal is to foster social-ecological urban research aimed at understanding these complex systems using a holistic, ecology of cities perspective while contributing to an ecology for cities that enhances urban sustainability and resilience. This goal is being met through four broad programmatic objectives: (1) use long-term observations and datasets to articulate and answer new questions requiring a long-term perspective; (2) develop and use predictive models and future-looking scenarios to help answer research questions; (3) employ existing urban ecological theory while articulating new theory; and (4) build transdisciplinary partnerships to foster resilience and enhance sustainability in urban ecosystems while educating urban dwellers of all ages and experiences. CAP IV research is organized around eight interdisciplinary questions and ten long-term datasets and experiments, and researchers are organized into eight Interdisciplinary Research Themes to pursue these long-term research questions.</abstract>
      <funding>NSF Awards: CAP I: DEB-9714833, CAP II: DEB-0423704, CAP III: DEB-1026865, CAP IV: DEB-1832016</funding>
      <award>
        <funderName>National Science Foundation</funderName>
        <funderIdentifier>https://ror.org/021nxhr62</funderIdentifier>
        <awardNumber>1832016</awardNumber>
        <title>LTER: CAP IV - Investigating urban ecology and sustainability through the lens of Urban Ecological Infrastructure</title>
        <awardUrl>https://nsf.gov/awardsearch/showAward?AWD_ID=1832016&amp;HistoricalAwards=false</awardUrl>
      </award>
      <relatedProject>
        <title>Multiscale effects of climate variability and change on hydrologic regimes, ecosystem function, and community structure in a desert stream and its catchment</title>
        <personnel>
          <individualName>
            <givenName>Nancy</givenName>
            <surName>Grimm</surName>
          </individualName>
          <organizationName>Arizona State University</organizationName>
          <electronicMailAddress>nbgrimm@asu.edu</electronicMailAddress>
          <userId directory="https://orcid.org">0000-0001-9374-660X</userId>
          <role>Principal Investigator</role>
        </personnel>
        <personnel>
          <individualName>
            <givenName>John</givenName>
            <surName>Sabo</surName>
          </individualName>
          <organizationName>Arizona State University</organizationName>
          <electronicMailAddress>john.l.sabo@asu.edu</electronicMailAddress>
          <userId directory="https://orcid.org">0000-0001-5259-0709</userId>
          <role>Co-Principal Investigator</role>
        </personnel>
        <abstract>
          <markdown>The primary objective of this project is to understand how long-term climate variability and change, as expressed through hydrologic regime shifts, influence the structure and function of desert streams. Desert streams are well suited for observing the consequences of climate variability because they experience high hydrologic variability at multiple scales. Researchers will determine how regime shifts influence 1) large-scale stream biogeomorphic structure (i.e., prevalence and persistence of wetlands) over multiple years via their influence on factors that control vegetation biomass, and 2) within-year successional patterns in ecosystem processes and community structure of primary producers and consumers of reaches along a gradient of wetland presence and stability. Arid regions are characterized by high interannual variation in precipitation, and these climate patterns drive the overall disturbance regime (in terms of flooding and drying), which influences the geomorphic structure and nutrient status of desert stream ecosystems. Embedded within the multi-annual hydrologic regime, flash floods scour stream channels and initiate a series of rapid successional changes by benthic algae, aquatic and wetland plants, and macroinvertebrates at short time scales (i.e., within a year); patterns of succession are hypothesized to vary among years within the long-term hydrologic regime. The research will use new techniques developed by the team to analyze the relationships between hydrologic variability, hydrologic regime shifts, and ecosystem and community properties.</markdown>
        </abstract>
        <funding>
          <markdown>
  - NSF SUCCESSION-I 1977-1979 DEB-7724478 (Fisher)
  - NSF SUCCESSION-II 1980-1983 DEB-8004145 (Fisher)
  - NSF SUCCESSION-III 1984-1987 BSR-8406891 (Fisher)
  - NSF Postdoc 1987-1989 BSR 87-00122 (Grimm)
  - NSF STABILITY - 1989-1992 BSR-8818612 (Fisher and Grimm)
  - NSF TROPHIC STRUCTURE 1990-1992 BSR-9008114 (Fisher, Grimm, and Dudley)
  - NSF LTREB I - 1991-1996 DEB-9108362 (Grimm and Fisher)
  - NSF HETEROGENEITY - 1993-1998 DEB-9306909 (Fisher and Grimm)
  - EPA HYPORHEIC - 1994-1996 #R821250-01-0 (Fisher and Grimm)
  - NSF LINX I - 1996-1999 - DEB-9628860 (subaward to Grimm, Marti, and Fisher)
  - NSF LTREB II - 1996-2001 DEB-9615358 (Grimm and Fisher)
  - NSF LINX II - 2001-2006 DEB-0111410 (subaward to Grimm and Dahm)
  - NSF LTREB III - 2009-2015 DEB-0918262 (Grimm and Sabo)
  - NSF LTREB IV - 2015-2020 DEB-1457227 (Grimm and Sabo)
  - NSF LINKAGES - 1998-2000 DEB-9727311 (Fisher and Grimm)
  - NSF DDIG - 1998-1999 DEB-9800912 (Dent and Grimm)
  </markdown>
        </funding>
        <award>
          <funderName>National Science Foundation</funderName>
          <funderIdentifier>https://ror.org/021nxhr62</funderIdentifier>
          <awardNumber>1457227</awardNumber>
          <title>LTREB Renewal: Multiscale effects of climate variability and change on hydrologic regimes, ecosystem function, and community structure in a desert stream and its catchment</title>
          <awardUrl>https://nsf.gov/awardsearch/showAward?AWD_ID=1457227&amp;HistoricalAwards=false</awardUrl>
        </award>
      </relatedProject>
    </project>
    <dataTable id="606_water_chemistry_cc0e23335c881f45a9032a5a94fb8556.csv">
      <entityName>606_water_chemistry_cc0e23335c881f45a9032a5a94fb8556.csv</entityName>
      <entityDescription>water chemistry (chloride, bromide, nitrate-nitrogen, ammonium-nitrogen, total dissolved nitrogen, dissolved organic nitrogen, soluble reactive phosphate) at selected times and locations in Sycamore Creek, AZ, USA</entityDescription>
      <physical>
        <objectName>606_water_chemistry_cc0e23335c881f45a9032a5a94fb8556.csv</objectName>
        <size unit="bytes">198314</size>
        <authentication method="MD5">cc0e23335c881f45a9032a5a94fb8556</authentication>
        <dataFormat>
          <textFormat>
            <numHeaderLines>1</numHeaderLines>
            <recordDelimiter>\r\n</recordDelimiter>
            <attributeOrientation>column</attributeOrientation>
            <simpleDelimited>
              <fieldDelimiter>,</fieldDelimiter>
              <quoteCharacter>"</quoteCharacter>
            </simpleDelimited>
          </textFormat>
        </dataFormat>
        <distribution>
          <online>
            <url function="download">https://data.gios.asu.edu/datasets/cap/606_water_chemistry_cc0e23335c881f45a9032a5a94fb8556.csv</url>
          </online>
        </distribution>
      </physical>
      <coverage>
        <temporalCoverage>
          <rangeOfDates>
            <beginDate>
              <calendarDate>2010-04-15</calendarDate>
            </beginDate>
            <endDate>
              <calendarDate>2014-06-24</calendarDate>
            </endDate>
          </rangeOfDates>
        </temporalCoverage>
      </coverage>
      <attributeList>
        <attribute>
          <attributeName>Date</attributeName>
          <attributeDefinition>date of sample collection</attributeDefinition>
          <storageType>date</storageType>
          <measurementScale>
            <dateTime>
              <formatString>YYYY-MM-DD</formatString>
              <dateTimeDomain/>
            </dateTime>
          </measurementScale>
        </attribute>
        <attribute>
          <attributeName>site</attributeName>
          <attributeDefinition>sampling location along Sycamore Creek</attributeDefinition>
          <storageType>string</storageType>
          <measurementScale>
            <nominal>
              <nonNumericDomain>
                <enumeratedDomain>
                  <codeDefinition>
                    <code>ABOVE RG</code>
                    <definition>above Round Valley gravel</definition>
                  </codeDefinition>
                  <codeDefinition>
                    <code>BG</code>
                    <definition>Below Mesquite Wash gravel</definition>
                  </codeDefinition>
                  <codeDefinition>
                    <code>BW</code>
                    <definition>Below Mesquite Wash wetland</definition>
                  </codeDefinition>
                  <codeDefinition>
                    <code>RG</code>
                    <definition>Round Valley gravel</definition>
                  </codeDefinition>
                  <codeDefinition>
                    <code>RV</code>
                    <definition>Round Valley</definition>
                  </codeDefinition>
                  <codeDefinition>
                    <code>RW</code>
                    <definition>Round Valley wetland</definition>
                  </codeDefinition>
                  <codeDefinition>
                    <code>SG</code>
                    <definition>Dos S Ranch gravel</definition>
                  </codeDefinition>
                  <codeDefinition>
                    <code>SW</code>
                    <definition>Dos S Ranch gravel</definition>
                  </codeDefinition>
                </enumeratedDomain>
              </nonNumericDomain>
            </nominal>
          </measurementScale>
        </attribute>
        <attribute>
          <attributeName>sample_id</attributeName>
          <attributeDefinition>Denotes location within the reach the sample was taken ISCO bottle number; or grab sample. DOWN (collected at downstream sample station within the reach); UP (collected at upstream sample station within the reach); Meter numbers (meters downstream from top of reach; taken during background sampling before nutrient injection study); FLOOD GRAB (grab sample taken during a flood near ISCO)</attributeDefinition>
          <storageType>string</storageType>
          <measurementScale>
            <nominal>
              <nonNumericDomain>
                <textDomain>
                  <definition>Denotes location within the reach the sample was taken ISCO bottle number; or grab sample. DOWN (collected at downstream sample station within the reach); UP (collected at upstream sample station within the reach); Meter numbers (meters downstream from top of reach; taken during background sampling before nutrient injection study); FLOOD GRAB (grab sample taken during a flood near ISCO)</definition>
                </textDomain>
              </nonNumericDomain>
            </nominal>
          </measurementScale>
          <missingValueCode>
            <code>NA</code>
            <codeExplanation>missing value</codeExplanation>
          </missingValueCode>
        </attribute>
        <attribute>
          <attributeName>replicate</attributeName>
          <attributeDefinition>sample replicate</attributeDefinition>
          <storageType>string</storageType>
          <measurementScale>
            <nominal>
              <nonNumericDomain>
                <enumeratedDomain>
                  <codeDefinition>
                    <code>1</code>
                    <definition>replicate number 1</definition>
                  </codeDefinition>
                  <codeDefinition>
                    <code>2</code>
                    <definition>replicate number 2</definition>
                  </codeDefinition>
                  <codeDefinition>
                    <code>3</code>
                    <definition>replicate number 3</definition>
                  </codeDefinition>
                </enumeratedDomain>
              </nonNumericDomain>
            </nominal>
          </measurementScale>
          <missingValueCode>
            <code>NA</code>
            <codeExplanation>missing value</codeExplanation>
          </missingValueCode>
        </attribute>
        <attribute>
          <attributeName>stream_flow</attributeName>
          <attributeDefinition>indicator of flow state: baseflow; flood</attributeDefinition>
          <storageType>string</storageType>
          <measurementScale>
            <nominal>
              <nonNumericDomain>
                <enumeratedDomain>
                  <codeDefinition>
                    <code>0</code>
                    <definition>baseflow</definition>
                  </codeDefinition>
                </enumeratedDomain>
              </nonNumericDomain>
            </nominal>
          </measurementScale>
          <missingValueCode>
            <code>NA</code>
            <codeExplanation>missing value</codeExplanation>
          </missingValueCode>
        </attribute>
        <attribute>
          <attributeName>analyte</attributeName>
          <attributeDefinition>analyte measured</attributeDefinition>
          <storageType>string</storageType>
          <measurementScale>
            <nominal>
              <nonNumericDomain>
                <enumeratedDomain>
                  <codeDefinition>
                    <code>ammonium-nitrogen</code>
                    <definition>ammonium-nitrogen</definition>
                  </codeDefinition>
                  <codeDefinition>
                    <code>bromide</code>
                    <definition>bromide</definition>
                  </codeDefinition>
                  <codeDefinition>
                    <code>chloride</code>
                    <definition>chloride</definition>
                  </codeDefinition>
                  <codeDefinition>
                    <code>nitrate-nitrogen</code>
                    <definition>nitrate-nitrogen</definition>
                  </codeDefinition>
                  <codeDefinition>
                    <code>soluble reactive phosphate</code>
                    <definition>soluble reactive phosphate</definition>
                  </codeDefinition>
                  <codeDefinition>
                    <code>total dissolved nitrogen</code>
                    <definition>total dissolved nitrogen</definition>
                  </codeDefinition>
                </enumeratedDomain>
              </nonNumericDomain>
            </nominal>
          </measurementScale>
        </attribute>
        <attribute>
          <attributeName>concentration</attributeName>
          <attributeDefinition>concentration of analyte measured</attributeDefinition>
          <storageType>float</storageType>
          <measurementScale>
            <ratio>
              <unit>
                <standardUnit>milligramPerLiter</standardUnit>
              </unit>
              <numericDomain>
                <numberType>real</numberType>
                <bounds>
                  <minimum exclusive="false">-0.168</minimum>
                  <maximum exclusive="false">35.77</maximum>
                </bounds>
              </numericDomain>
            </ratio>
          </measurementScale>
        </attribute>
        <attribute>
          <attributeName>flag</attributeName>
          <attributeDefinition>analytical quality control or quality assurance flag</attributeDefinition>
          <storageType>string</storageType>
          <measurementScale>
            <nominal>
              <nonNumericDomain>
                <enumeratedDomain>
                  <codeDefinition>
                    <code>1</code>
                    <definition>suspect</definition>
                  </codeDefinition>
                  <codeDefinition>
                    <code>2</code>
                    <definition>below detection limit</definition>
                  </codeDefinition>
                </enumeratedDomain>
              </nonNumericDomain>
            </nominal>
          </measurementScale>
          <missingValueCode>
            <code>NA</code>
            <codeExplanation>missing value</codeExplanation>
          </missingValueCode>
        </attribute>
      </attributeList>
      <numberOfRecords>3370</numberOfRecords>
    </dataTable>
  </dataset>
</eml:eml>
