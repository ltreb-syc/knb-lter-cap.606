*Baseline samples:*

Collected both upstream (of spring sequence injection point) and downstream of each gravel and wetland study reach. Samples were filtered in the lab using ashed Whatman 47mm GFF filters. Filtrate was stored in Falcon tubes and frozen until analyzed.

*Spring sequence samples (April-July):*

NO3, SRP samples were field filtered using an ashed Whatman GFF (2010-2011) or a .45 um membrane syringe filter (since 2012). DOC/TN samples were field-filtered using an ashed Whatman GFF filter. Filtrate was stored in Falcon tubes and frozen until analyzed.  NO3, NH4, and SRP were analyzed using flow injection analysis (Lachat QC8000). DOC and TN were analyzed using total organic carbon/nitrogen analysis (Shimadzu TOV-C). Known standard concentrations and blank samples were run every 10 field samples to ensure quality control.
